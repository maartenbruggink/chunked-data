/**
 * @function Update specific Algolia entries
 *
 * @param req - Node request object
 * @param res - Node response object
 */
export default async (req, res) => {
	const bodyData = req.body || {};

	return res.end(
		JSON.stringify({
			success: true,
			message: bodyData,
		})
	);
};
